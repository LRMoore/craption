""" Unit tests for recordings, playback and the recordings database class

    Requires microphone
"""
import argparse
import unittest

import sqlalchemy as sa

from craption.recording import Recording, RecordingDatabase
import craption.config as cfg


### --- main script

class RecordingTestCase(unittest.TestCase):
    def setUp(self):
        engine = sa.create_engine(f'sqlite:///{cfg.database_path}')
        self.con = engine.connect()
        self.rec = Recording()

    #def test_record(self):
    #    self.rec.record()
    #    self.assertGreater(self.rec.duration, 0.)

    def test_record_playback(self):
        with self.subTest():
            self.rec.record()
            self.assertGreater(self.rec.duration, 0.)
        with self.subTest():
            self.rec.play()

    def test_update_transcript(self):
        self.rec.user_transcript = """
        hello this is a purposefully long sentence which i intend to use to
        calibrate the linebreaks on this display thing. i need this to be 3
        or so lines to understand if it's working properly ok.
        """

    def test_print_info(self):
        print(self.rec)

    def test_tag_change(self):
        prev_path = self.rec.path
        self.rec.tag = 'new_tag'
        self.assertNotEqual(prev_path, self.rec.path)
        print(self.rec)

    def test_add_recording(self):
        with self.subTest():
            db = RecordingDatabase(db=self.con)
        with self.subTest():
            # test recording
            r = Recording()
            r.record()
            db.add_recording(r)

    def test_database_load(self):
        db = RecordingDatabase(db=self.con)
        db.load()

    def test_database_save(self):
        db = RecordingDatabase(db=self.con)
        db.save()

    def test_reconstruct_from_db(self):
        db = RecordingDatabase(db=self.con)
        if not db._df.empty:
            row = db._df.iloc[0].name
            r = db.get_recording(row)
            r.play()


if __name__ == '__main__':
    unittest.main()
