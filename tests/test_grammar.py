""" Unit tests for grammatical analysis of text with NLTK
"""

import argparse
import unittest
import string
import sys

import sqlalchemy as sa
import nltk

import craption
import craption.config as cfg

from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize, word_tokenize

from craption.analysis import ParsedText


# ensure NLTK language analysis resources are available
print("Checking whether NLTK resources are downloaded...")
#nltk.download()


class AnalysisTestCase(unittest.TestCase):

    def setUp(self):
        self.parsed_text = ParsedText("this’s a sent tokenize test. this is "
                                      "sent two. is this sent three? sent 4 "
                                      "is cool! Now it’s your turn.")

    def test_grammar_tagging(self):
        print(self.parsed_text.tagged_sentences)


if __name__ == '__main__':
    unittest.main()

    #text = "this’s a sent tokenize test. this is sent two. is this sent three? sent 4 is cool! Now it’s your turn."

    #pt = ParsedText(text)
    #print(pt.tagged_sentences)
    """
    sentence_tokenize_list = sent_tokenize(text)
    word_tokenize_list = sorted(
        list(set(w for w in word_tokenize(text) if len(w) > 1 or w == 'a'))
    )

    print("sentences:",sentence_tokenize_list)
    print("words:", word_tokenize_list)



    # Get a collection of synsets (synonym sets) for a word
    synsets = wordnet.synsets( 'picture' )
    print(synsets)
    # Print the information
    for synset in synsets:
        print("-" * 10)
        print("Name:", synset.name())
        print("Lexical Type:", synset.lexname())
        print("Synonyms:", synset.lemma_names())
        print("Definition:", synset.definition())
        print("Lemma names:", synset.lemma_names())
        print("Hyponyms:", [h.name().split('.')[0] for h in synset.hyponyms()])
        print("Hypernyms:", [h.name().split('.')[0] for h in synset.hypernyms()])
        print("Meronyms", [h.name().split('.')[0] for h in synset.part_meronyms()])
        print("Holonyms:", [h.name().split('.')[0] for h in synset.part_holonyms()])
        print("Entailments:", [h.name().split('.')[0] for h in synset.entailments()])
        for example in synset.examples():
            print("Example:", example)
    """
