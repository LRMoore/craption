""" Unit tests for Transcriber: the DeepSpeech model wrapper
"""

import argparse
import unittest
import os
import sqlalchemy as sa

from craption.transcribe import Transcriber
from craption.recording import Recording, RecordingDatabase
import craption.config as cfg


class TranscribeTestCase(unittest.TestCase):

    def test_load_model(self):
        Transcriber()

    def test_transcribe_recording(self):
        t = Transcriber()
        t.transcribe_audio(os.path.join(cfg.recording_dir, 'test.wav'))


if __name__ == '__main__':
    unittest.main()
