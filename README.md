# craption

`craption` is an interactive speech-to-text python application built on Mozilla DeepSpeech, an open-source deep learning speech-to-text library written in TensorFlow.

It supports the persistence of speech and parsing results using an SQL database.

`craption` has been tested and confirmed to function on:

- Ubuntu 18.04
- macOS Mojave version 10.14.5

# Installation

It is recommended to install `craption` using [Anaconda](https://www.anaconda.com/distribution/ "Anaconda Homepage"), which can conveniently bootstrap a working tensorflow-gpu environment.

The installation is broken down into 5 steps, with divergent instructions where appropriate for Ubuntu or macOS users.

### 1 - Install (Ana/mini)conda

#### Ubuntu
Download the latest Anaconda version from 
```
https://www.anaconda.com/download/
```
and run the install script, e.g.
```
cd /tmp
curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
bash Anaconda3-2019.03-Linux-x86_64.sh
```

#### macOS

It is recommended to use the [homebrew](https://brew.sh/) package manager.
To install it, paste in a terminal prompt:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Then use brew to install anaconda (will need a sudo password):
```bash
brew cask install anaconda
```
If at this point the `conda` command is not found, export the path (assuming a bash shell):
```
export PATH="/usr/local/anaconda3/bin:$PATH"
```
and run
```bash
conda init
```

### 2 - conda environment (tensorflow-gpu + python 3.6)

#### Ubuntu/macOS

Deepspeech requires python *3.6* and comes with a GPU-accelerated version. Depending on hardware, either:

install the GPU version:

```
conda create -n craption tensorflow-gpu python=3.6
```

or the regular CPU-only module:

```
conda create -n craption tensorflow python=3.6
```

The GPU version is much faster and therefore recommended,
but both versions have been tested successfully.

Once set up, ensure that you're in the environment:

```
conda activate craption
```

You can test tensorflow-gpu is working by entering your Python interpreter and running:

```
import tensorflow as tf
tf.Session()
```
which should pass without incident.

### 3 - Dependencies in conda forge

#### Ubuntu 
We install the Natural Language Toolkit (nltk) and Jupyter from
 standard conda channels.

To avoid encountering "no device" type errors in PyAudio, 
install the following patched versions of the
sound library dependencies (portaudio and pyaudio)

```
conda install nltk jupyter
conda install nwani::portaudio nwani::pyaudio
```

#### macOS
The standard conda channels can be used for all dependencies.
```
conda install nltk jupyter portaudio pyaudio
```

### 4 - craption application install

#### Ubuntu/macOS
Clone the repo and cd into the project directory.
```
git clone https://bitbucket.org/LRMoore/craption.git
cd craption
```

Now, install the rest of the dependencies from the requirements file.
```
pip install -r requirements.txt
```

and install the required Natural Language Toolkit (nltk) files with
```
python -m nltk.downloader popular
```

### 5 - Install Deepspeech and its python dependencies

The open-source Mozilla DeepSpeech package must be installed, which comes with a pre-trained model (~2GB).

#### Ubuntu
For convenience, an installer script is included.
```
chmod 755 ./install_deepspeech.sh
./install_deepspeech.sh
```

#### macOS
Note - the DeepSpeech directory should be inside the directory of the cloned `craption` repository.
```
git clone https://github.com/mozilla/DeepSpeech
cd DeepSpeech
curl https://github.com/mozilla/DeepSpeech/releases/download/v0.5.1/deepspeech-0.5.1-models.tar.gz -O
tar xfz deepspeech-0.5.1-models.tar.gz
```

Then install the `deepspeech-gpu` python module if using a GPU
```
pip install deepspeech-gpu 
cd ..
```

... or use `deepspeech` if using a CPU
```
pip install deepspeech
cd ..
```

This completes the installation!

### 6 (Optional) test deepspeech is working correctly from a terminal

We will require a spoken voice sample, which should have a 16kHz sample rate, be mono-channel, and use 16 bit sample recording.

If using macOS, ensure `sox` is installed first with
```bash
brew install sox
```

Record yourself saying something (stop with ctrl+C) with:
```
rec -c 1 -r 16000 -b 16 my_voice.wav
```

Assuming the downloaded models are stored in `./DeepSpeech/deepspeech-0.5.1-models`, do:

```
deepspeech --model ./DeepSpeech/deepspeech-0.5.1-models/output_graph.pbmm --alphabet ./DeepSpeech/deepspeech-0.5.1-models/alphabet.txt --lm ./DeepSpeech/deepspeech-0.5.1-models/lm.binary --trie ./DeepSpeech/deepspeech-0.5.1-models/trie --audio my_voice.wav
```

This should print an approximation of what you just said to the terminal at the bottom of a long stream of debug output.

The first time running this will take order ~2m to prepare the model, and transcribes a sentence of speech in a few seconds on a GTX 1050Ti (4GB VRAM), and about 15 seconds on a 2015 Intel i7 CPU.


# Running Craption

From within the `craption` directory, just do:

```
python app.py
```
to enter the interactive shell, or if you have already installed the nltk dependencies during the craption install, do

```
python app.py -n
```

`craption` has extensive in-app help, accessible
via the help commands. Just hit the appropriate commands (or their [s]hortcuts),
and press enter!

It can:

  - Record and playback your voice as .wav files tracked and managed by an SQL database
  - Handle metadata entry and storage such as user-provided tags, user-provided transcripts
  and speaker names.
  - Transcribe your speech samples with DeepSpeech for comparison.
  - Parse your sentence grammar with NLTK

It also assumes you have a microphone connected which PyAudio can recognise.

Enjoy!

## screenshots

### launch

![screenshot](https://bitbucket.org/LRMoore/craption/raw/992ac932cd4626a7908fe8b90fef4a2a276085c0/main_menu.png)

### recording voice

![screenshot](https://bitbucket.org/LRMoore/craption/raw/b69cda64eb34276f14f32458058418e77610a11e/screen_rec.png)

### adding metadata and inferring transcript

![screenshot](https://bitbucket.org/LRMoore/craption/raw/b69cda64eb34276f14f32458058418e77610a11e/screen_transcript.png)

### parsing grammar

![screenshot](https://bitbucket.org/LRMoore/craption/raw/b69cda64eb34276f14f32458058418e77610a11e/screen_grammar.png)

## unittests

You can run the individual unittests with:

```
cd craption
python -m unittest tests/test_transcribe.py
python -m unittest tests/test_grammar.py
python -m unittest tests/test_recording.py
```
