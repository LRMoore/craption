#!/usr/bin/env python
# coding=utf-8
import argparse
import os

import tensorflow as tf
import sqlalchemy as sa
import nltk

import craption.config as cfg

from craption.transcribe import Transcriber
from craption.recording import RecordingDatabase
from craption.menu import MainMenu

# suppress TensorFlow output spam
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

### --- setup script arguments
parser = argparse.ArgumentParser(description=__doc__,
 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# path to database file
parser.add_argument('-db', '--db-path', dest='db_path', type=str,
                    default=cfg.database_path,
                    help='sql database  path, e.g.: "./metadata.db"')
parser.add_argument('-n', '--nltk-downloaded', dest='nltk_downloaded',
                    action='store_true', default=False,
                    help='flags whether nltk.download() has been run already')
args = parser.parse_args()


if __name__ == '__main__':

    if not args.nltk_downloaded:
        print("You need to download NLTK's toolkits to run the language parsing.")
        print("A window will now open where you can do this!")
        print("If you've done this already, you can run this app with the -n flag to skip.")
        print("Just close the window if/once the modules are installed (Ctrl+X).")
        nltk.download()

    # establish a connection to the recording metadata database
    engine = sa.create_engine(f'sqlite:///{args.db_path}')
    con = engine.connect()
    db = RecordingDatabase(db=con)
    # load the DeepSpeech STT model
    t = Transcriber()
    # start an interactive app session
    root = MainMenu(db=db, transcriber=t)
    root.cmdloop()
