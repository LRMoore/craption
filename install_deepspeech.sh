PARENT_DIR=$(pwd)
DEEPSPEECH_DIR="$PARENT_DIR/DeepSpeech"
MODEL_DIR='deepspeech-0.5.1-models'
MODEL_TAR="$MODEL_DIR.tar.gz"
MODEL_URL="https://github.com/mozilla/DeepSpeech/releases/download/v0.5.1/$MODEL_TAR"

# Install Git Large File Storage
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash

# Clone the DeepSpeech repository
if [ -d $DEEPSPEECH_DIR ]; then
  echo "$DEEPSPEECH_DIR directory already present, skipping download..."
else
  git clone https://github.com/mozilla/DeepSpeech
fi

# and unpack the pre-trained model (this takes a while):
cd $DEEPSPEECH_DIR
if [ -d $MODEL_DIR ]; then
  echo "$MODEL_DIR already present, skipping download..."
else
  wget -O- $MODEL_URL | tar xvfz -
fi

# ensure dependencies
sudo apt update
sudo apt install build-essential libssl-dev libffi-dev
sudo apt install sox

# install deepspeech wheels with gpu support
pip install deepspeech-gpu
pip install --upgrade deepspeech-gpu
