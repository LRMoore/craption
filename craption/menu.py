# coding=utf-8
"""
Main CLI with nested command structure.

Maps user input to actions such as recording audio snippets, browsing recorded
samples and transcribing/analysing them grammatically etc.
"""
import sys
import argparse
import random
import sys

import tabulate
import textwrap
import cmd2
import cmd2_submenu

from IPython import embed

import craption.config as cfg

from craption.analysis import ParsedText, pos_tag_meanings
from craption.transcribe import Transcriber
from craption.recording import Recording, RecordingDatabase


total_columns = cfg.total_columns

# colours for text
DARK_RED = "\033[0;31m"
RED = "\033[1;31m"
YELLOW = "\033[1;33m"
WHITE = "\033[1;37m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"
BOLD = "\033[;1m"
REVERSE = "\033[;7m"


def pprompt(text, n_rows_padding=1, pad_char='*', side_padding=3):
    """ Prettify and center prompt text in red for menus
    """
    pad_txt = side_padding * pad_char
    n_cols = cfg.total_columns - 2*side_padding
    for _ in range(n_rows_padding):
        sys.stdout.write(GREEN + pad_txt + ''.center(n_cols, ' ') + pad_txt + '\n')
    sys.stdout.write(GREEN + pad_txt + RED + text.center(n_cols, ' ') + GREEN + pad_txt +'\n')
    for _ in range(n_rows_padding):
        sys.stdout.write(GREEN + pad_txt + ''.center(n_cols, ' ') + pad_txt + '\n')


class InspectMenu(cmd2.Cmd):
    """ Menu class for manipulating recordings, examining and assigning
        their properties.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.intro = GREEN + """

                        Welcome to the inspect menu.

                        Options:

                        (i)nfo       : print information on current recording
                        (d)eepspeech : use DeepSpeech to infer transcription of current recording
                        (p)layback   : play the current sample back
                        (s)peaker    : assign a speaker to the recording
                        (t)ag        : update the tag of the recording
                        t(r)anscribe : offer a transcription of the current sample
                        (g)rammar    : analyse the grammar of the current sample
                        (l)ist       : list all recorded samples in database
                        s(e)lect     : select another sample by id
                        sa(v)e       : save the currently selected sample
                        p(o)s        : display information to help interpret parts-of-speech tags

                        (c)ommands   : re-display the intro message
                        help         : list detailed help options and commands
                        quit (Ctrl+D): back to the main menu
        """
        self.db = None
        self.current_rec = None
        self.transcriber = None
        self.prompt = YELLOW + "Inspect " + GREEN

    # define arguments to "playback" command
    playback_parser = argparse.ArgumentParser()

    @cmd2.with_argparser(playback_parser)
    def do_playback(self, args):
        """ plays the current recording back with PyAudio """
        if self.current_rec is None:
            pprompt("No recording currently selected!")
            return
        pprompt(f"Playing back {self.current_rec.unique_tag}...")
        self.current_rec.play()
    # aliases
    do_p = do_playback


    # define arguments to "annotate" command
    speaker_parser = argparse.ArgumentParser()
    speaker_parser.add_argument('speaker', type=str, default='', nargs='?',
                                 help='name of speaker')

    @cmd2.with_argparser(speaker_parser)
    def do_speaker(self, args):
        """ prompts the user to enter the speaker in the current recording """
        if self.current_rec is None:
            pprompt("No recording currently selected!")
            return
        if args.speaker:
            self.current_rec.speaker = args.speaker
        else:
            speaker = input("Enter the speaker: ")
            self.current_rec.speaker = speaker
        pprompt(f"{self.current_rec.unique_tag} speaker set to "
                f"{self.current_rec.speaker}")
    # aliases
    do_s = do_speaker

    # define arguments to "info" command
    playback_parser = argparse.ArgumentParser()

    @cmd2.with_argparser(playback_parser)
    def do_info(self, args):
        """ displays information on the current recording """
        print(self.current_rec)
    # aliases
    do_i = do_info

    # define arguments to "transcribe" command
    transcribe_parser = argparse.ArgumentParser()
    transcribe_parser.add_argument('usertranscript', type=str, default='', nargs='?',
                                 help='accurate human transcription of the audio')

    @cmd2.with_argparser(transcribe_parser)
    def do_transcribe(self, args):
        """ prompts the user to enter a transcription of the current recording
        """
        if self.current_rec is None:
            pprompt("No recording currently selected!")
            return
        if args.usertranscript:
            self.current_rec.user_transcript = args.usertranscript
        else:
            trn = input("Enter the transcript of the recording: ")
            self.current_rec.user_transcript = trn
        pprompt(f"{self.current_rec.unique_tag} transcript set to: ")
        lines = textwrap.wrap(self.current_rec.user_transcript,
                              width=cfg.total_columns)
        for l in lines:
            print(l)
    # aliases
    do_r = do_transcribe


    # define arguments to "select" command
    select_parser = argparse.ArgumentParser()
    select_parser.add_argument('uniquetag', type=str, nargs='?',
                               help='select a recording based on its unique_tag')

    @cmd2.with_argparser(select_parser)
    def do_select(self, args):
        """ changes the current recording to another, identified by unique_tag
        """
        if not args.uniquetag:
            utag = input("Enter the unique_tag of the desired recording: ")
        else:
            utag = args.uniquetag
        try:
            self.current_rec = self.db.get_recording(utag)
        except KeyError:
            pprompt(f"No recording matches unique tag: {utag}!")
    # aliases
    do_e = do_select


    # define arguments to "tag" command
    tag_parser = argparse.ArgumentParser()
    tag_parser.add_argument('tag', type=str, nargs='?',
                           help='assign a tag to a recording')

    @cmd2.with_argparser(tag_parser)
    def do_tag(self, args):
        """ updates the tag of the current recording """
        if self.current_rec is None:
            pprompt("No recording currently selected!")
            return
        if not args.tag:
            tag = input("Enter the desired tag for the recording: ")
        else:
            tag = args.tag
        self.current_rec.tag = tag
        pprompt(f"Current recording was assigned tag: {self.current_rec.tag}")
    # aliases
    do_t = do_tag

    # define arguments to "list" command
    list_parser = argparse.ArgumentParser()

    @cmd2.with_argparser(list_parser)
    def do_list(self, args):
        """ Displays a list of the saved recordings """
        pprompt("The available recordings are:")
        # try paginated print first, falling back on regular one if this causes
        # problems
        try:
            self.ppaged(self.db)
        except:
            print(self.db)
        finally:
            print("\n")
    # assign aliases
    do_l = do_list

    # define arguments to "save" command
    save_parser = argparse.ArgumentParser()

    @cmd2.with_argparser(save_parser)
    def do_save(self, args):
        """ saves the current recording """
        if self.current_rec is None:
            pprompt("No recording currently selected!")
            return
        self.db.add_recording(self.current_rec)
        self.db.save()
        pprompt(f"{self.current_rec.unique_tag} metadata saved to database!")
    # assign aliases
    do_v = do_save

    # define arguments to "deepspeech" command
    deepspeech_parser = argparse.ArgumentParser()

    @cmd2.with_argparser(deepspeech_parser)
    def do_deepspeech(self, args):
        if self.current_rec is None:
            pprompt("No recording currently selected!")
            return
        text = self.transcriber.transcribe_audio(self.current_rec)
        pprompt(f"Inferred transcript for {self.current_rec.unique_tag}: ")
        lines = textwrap.wrap(self.current_rec.predicted_transcript,
                              width=cfg.total_columns)
        for l in lines:
            print(l)
    do_d = do_deepspeech

    # define arguments to "grammar" command
    grammar_parser = argparse.ArgumentParser()
    grammar_parser.add_argument('-u','--usertranscript',
                                action='store_true', default=False,
                                help=('examine user-provided transcript '
                                     '(rather than inferred by deepspeech)'))
    @cmd2.with_argparser(grammar_parser)
    def do_grammar(self, args):
        if self.current_rec is None:
            pprompt("No recording currently selected!")
            return
        if args.usertranscript:
            if self.current_rec.user_transcript is None:
                pprompt("No user transcript provided!")
                return
            pt = ParsedText(self.current_rec.user_transcript)
        else:
            if self.current_rec.predicted_transcript is None:
                pprompt("Transcript not yet predicted. Run (d)eepspeech first!")
                return
            pt = ParsedText(self.current_rec.predicted_transcript)
        pprompt("Guessed grammatical deconstruction of the text as:")
        for s in pt.tagged_sentences:
            print(s)
    # aliases
    do_g = do_grammar

    # define arguments to "pos" command
    pos_parser = argparse.ArgumentParser()

    @cmd2.with_argparser(pos_parser)
    def do_pos(self, args):
        """ show information to help interpret Parts-of-Speech tags """
        for k, v in pos_tag_meanings.items():
            print(f'{k} : {v}')
    # aliases
    do_o = do_pos

    def do_commands(self, args):
        """ re-display intro message """
        print(self.intro)
    # alias
    do_c = do_commands

@cmd2_submenu.AddSubmenu(InspectMenu(),
                 command='inspect',
                 aliases=('i','I'),
                 shared_attributes=dict(db='db',
                                        current_rec='current_rec',
                                        transcriber='transcriber'))
class MainMenu(cmd2.Cmd):
    """The main / top level menu class that contains other submenus."""

    def __init__(self, db, transcriber, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.intro = GREEN + """

                        Welcome to the main menu.

                        Options:
                        [r]ecord        : record an audio sample via mic now
                        [l]ist          : list existing recordings
                        [i]nspect [tag] : inspect and analyse current [a given] recording

                        (c)ommands   : re-display the intro message
                        help (cmd) : list detailed help options (of cmd)
                        quit (Ctrl+D) : quit

         """
        self.prompt = CYAN + "Main " + GREEN
        self.maxrepeats = 3
        self.top_level_attr = 123456789
        self.current_rec = None
        self.db = db
        self.transcriber = transcriber

    # define arguments to "record" command
    rec_parser = argparse.ArgumentParser()
    rec_parser.add_argument('-p', '--playback', action='store_true', default=False,
                            help='play back the sample after it is recorded')
    rec_parser.add_argument('-t', '--tag', type=str, default='',
                            help='a tag to identify recording e.g. "my_voice"')
    rec_parser.add_argument('-s', '--speaker', type=str, default='',
                            help='a tag to identify recording e.g. "my_voice"')

    @cmd2.with_argparser(rec_parser)
    def do_record(self, args):
        """ record an audio sample """
        if args.tag:
            self.current_rec = Recording(args.tag)
        else:
            self.current_rec = Recording()
        pprompt("Now recording: say some words and hit Ctrl+C when done...")
        self.current_rec.record()
        pprompt(f"{self.current_rec.unique_tag} successfully recorded at "
                f"{self.current_rec.path}!")
        if args.playback:
            pprompt(f"Playing back {self.rec.unique_tag}...")
            self.current_rec.play()
    # assign aliases
    do_r = do_record


    # define arguments to "list" command
    list_parser = argparse.ArgumentParser()

    @cmd2.with_argparser(list_parser)
    def do_list(self, args):
        """Repeats what you tell me to."""
        pprompt("The available recordings are:")
        # try paginated print first, falling back on regular one if this causes
        # problems
        try:
            self.ppaged(self.db)
        except:
            print(self.db)
        finally:
            print("\n")
    # assign aliases
    do_l = do_list

    def do_commands(self, args):
        """ re-display intro message """
        print(self.intro)
    # alias
    do_c = do_commands

    def do_ipy(self, arg):
        """Enters an interactive IPython shell.
        Run python code from external files with ``run filename.py``
        End with ``Ctrl-D`` (Unix) / ``Ctrl-Z`` (Windows), ``quit()``, '`exit()``.
        """
        banner = 'Entering an embedded IPython shell type quit() or <Ctrl>-d to exit ...'
        exit_msg = 'Leaving IPython, back to {}'.format(sys.argv[0])
        embed(banner1=banner, exit_msg=exit_msg)

    #def help_record(self):
    #    print("useful instructions")
    #def complete_record(self, text, line, begidx, endidx):
    #    return [s for s in ['qwe', 'asd', 'zxc'] if s.startswith(text)]
