"""
These settings which are used globally as defaults, and include options
for output formatting, directories and http request timeout parameters.
"""

import logging
import os.path
from pathlib import Path

###################################################################
#
###################################################################

# Set logging verbosity
verbosity = 'DEBUG'
if isinstance(verbosity, str):
    try:
        verbosity = int(logging.getLevelName(verbosity))
    except ValueError:
        print('CONFIG ERROR: invalid verbosity in user config, '
              'valid includes INFO, DEBUG etc.')
        raise

# Set path for persisted audio recordings
recording_dir = './media'
try:
    recording_dir = Path(recording_dir)
except ValueError:
    print('CONFIG ERROR: invalid path.')
    raise

# Set path for audio recording metadata database
database_path = './metadata.db'
try:
    database_path = Path(database_path)
except ValueError:
    print('CONFIG ERROR: invalid path.')
    raise

###################################################################
# Stdout settings
###################################################################
# Column widths of logging output
total_columns = 100
caller_padding = 20
time_padding = 10

has_displayed_welcome = False

###################################################################
# Request settings
###################################################################
# All in seconds:
retry_interval = 5
# Default request timeout for aiohttp clientsession
default_global_request_timeout = 30
