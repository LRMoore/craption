import datetime
import logging
import sys

import craption.tools.logging
import craption.config as cfg
import craption.tools.technicolor as technicolor
import craption.tools.welcome as welcome

__name__ = 'craption'
__version__ = '1.0'

sys.path.insert(0, "..")

##########################################################################
# Logging, needs to be at top package level.
##########################################################################
# make the central log. All logs created in submodules
# with logging.getLogger will utilise the streamer and
# formatting of this log.
"""
log = logging.getLogger('craption')
sh = technicolor.ColorisingStreamHandler(sys.stdout)
f = craption.tools.logging.LogFormatter(
    '{asctime} | {name}.{funcName} | {message}',
    '%H:%M:%S', style='{'
)
sh.setFormatter(f)
log.setLevel(cfg.verbosity)
log.addHandler(sh)
log.headed_info = craption.tools.logging.headed_log(log.info)
log.headed_debug = craption.tools.logging.headed_log(log.debug)
"""
# Make an identical logger that all LoggableMixin classes will use and alter.
# This preserves the names of the submodule loggers.
log2 = logging.getLogger('classlog')
sh = technicolor.ColorisingStreamHandler(sys.stdout)
#log2.addHandler(sh)
f = craption.tools.logging.LogFormatter(
    '{asctime} | {name}.{funcName} | {message}',
    '%H:%M:%S', style='{'
)
sh.setFormatter(f)
log2.setLevel(cfg.verbosity)
log2.addHandler(sh)
#log2.headed_info =  craption.tools.logging.headed_log(log2.info)
#log2.headed_debug = craption.tools.logging.headed_log(log2.debug)

def set_verbosity(level):
    #log.setLevel(level)
    log2.setLevel(level)

##########################################################################
# Package welcome messages.
##########################################################################
craption.tools.welcome.welcome_message()
