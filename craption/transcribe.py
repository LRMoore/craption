""" Provides classes to interface to Mozilla Deepspeech for text transcription
    and utility functions for normalising .wav files
"""
import argparse
import numpy as np
import shlex
import os
import subprocess
import sys
import wave
import tensorflow as tf

from deepspeech import Model, printVersions
from timeit import default_timer as timer

try:
    from shhlex import quote
except ImportError:
    from pipes import quote

from craption.tools.logging import LoggableMixin
from craption.recording import Recording

# suppress TensorFlow output spam
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# utility functions for manipulating streams
def convert_samplerate(audio_path):
    sox_cmd = 'sox {} --type raw --bits 16 --channels 1 --rate 16000 --encoding signed-integer --endian little --compression 0.0 --no-dither - '.format(quote(audio_path))
    try:
        output = subprocess.check_output(shlex.split(sox_cmd), stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        raise RuntimeError('SoX returned non-zero status: {}'.format(e.stderr))
    except OSError as e:
        raise OSError(e.errno, 'SoX not found, use 16kHz files or install it: {}'.format(e.strerror))

    return 16000, np.frombuffer(output, np.int16)


def metadata_to_string(metadata):
    return ''.join(item.character for item in metadata.items)


class Transcriber(LoggableMixin):
    """
    Class to interface app to DeepSpeech model and generate transcriptions of
    audio samples

    Args:
        model_base_directory : directory containing model files
        model_binary, language_binary : required model components
        alphabet, trie : language-specific model components

        beam_width : Beam width used in the CTC decoder when building candidate
        transcriptions
        lm_alpha : The alpha hyperparameter of the CTC decoder. Language Model weight
        lm_beta : The beta hyperparameter of the CTC decoder. Word insertion bonus.

        # These constants are tied to the shape of the graph used (changing them changes
        # the geometry of the first layer), so make sure you use the same constants that
        # were used during training

        n_features : Number of MFCC features to use
        n_context : Size of the context window used for producing timesteps in the input vector

        extended : whether to output additional inferred text metadata
    """
    def __init__(self,
                 model_base_directory='./DeepSpeech/deepspeech-0.5.1-models/',
                 model_binary='output_graph.pbmm',
                 language_binary='lm.binary',
                 alphabet='alphabet.txt',
                 trie='trie',
                 beam_width=500,
                 lm_alpha=0.75,
                 lm_beta=1.85,
                 n_features=26,
                 n_context=9,
                 extended=False):
        self.log.debug("configuring Transcriber...")
        self.model_base_directory = model_base_directory
        self.model_binary = os.path.join(model_base_directory, model_binary)
        self.language_binary = os.path.join(model_base_directory, language_binary)
        # optional language binary and alphabet files
        # require both or none
        if bool(alphabet) ^ bool(trie):
            raise ValueError("Alphabet config file requires language model "
                             "binary file and vice versa!")
        else:
            self.alphabet = os.path.join(model_base_directory, alphabet)
            self.trie = os.path.join(model_base_directory, trie)
        self.beam_width = beam_width
        self.lm_alpha = lm_alpha
        self.lm_beta = lm_beta
        self.n_features = n_features
        self.n_context = n_context
        self._model = None
        self.extended = extended
        self.load_model()
        self.log.debug("Transcriber set up successfully")

    @property
    def model(self):
        return self._model

    def load_model(self) -> None:
        """ Loads a pre-trained DeepSpeech model at the specified paths and with
            hyperparameters taken from the instance
        """
        self.log.debug(f'Loading model from file {self.model_binary}')
        model_load_start = timer()
        try:
            self._model = Model(self.model_binary,
                                self.n_features,
                                self.n_context,
                                self.alphabet,
                                self.beam_width)
        except Exception as e:
            self.log.error(f"Could not load model in {self.model_base_directory}!")
            self.log.error(e)
            raise
        model_load_end = timer() - model_load_start
        self.log.debug('Loaded model in {:.3}s.'.format(model_load_end))
        if self.trie is not None:
            self.load_language_components()

    def load_language_components(self) -> None:
        """ Loads the language-specific model components of a DeepSpeech model
            located at the specified paths
        """
        self.log.debug(
            f'Loading language model from files {self.language_binary} & {self.trie}'
        )
        lm_load_start = timer()
        try:
            self.model.enableDecoderWithLM(self.alphabet,
                                           self.language_binary,
                                           self.trie,
                                           self.lm_alpha,
                                           self.lm_beta)
        except Exception as e:
            self.log.error(f"Could not load language model components in "
                           f"{self.model_base_directory}")
            self.log.error(e)
            raise
        lm_load_end = timer() - lm_load_start
        self.log.debug('Loaded language model in {:.3}s.'.format(lm_load_end))

    def transcribe_audio(self, rec) -> str:
        """ Infer the speech from an audio sample:

            Args:
                rec : string/path-like file object or recording.Recording instance
            Returns:
                a single string with the inferred text to match the audio
        """
        if isinstance(rec, Recording):
            path = rec.path
        else:
            path = rec
        fin = wave.open(path, 'rb')
        fs = fin.getframerate()
        if fs != 16000:
            self.log.warning(f'Warning: original sample rate ({fs}) is different '
                             'than 16kHz. Resampling might produce erratic '
                             'speech recognition.')
            fs, audio = convert_samplerate(path)
        else:
            audio = np.frombuffer(fin.readframes(fin.getnframes()), np.int16)

        audio_length = fin.getnframes() * (1/16000)
        fin.close()

        self.log.info('Running text inference with DeepSpeech...')
        inference_start = timer()
        if self.extended:
            text = metadata_to_string(self.model.sttWithMetadata(audio, fs))
        else:
            text = self.model.stt(audio, fs)
        inference_end = timer() - inference_start
        self.log.debug(f'Inference took {inference_end:0.3f}s for a '
                       f'{audio_length:0.3f}s audio file.')
        self.log.debug("Inferred text:")
        self.log.debug(text)
        if isinstance(rec, Recording):
            rec.predicted_transcript = text
        return text
