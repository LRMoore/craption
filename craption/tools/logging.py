# -*- coding: utf-8 -*-
"""
logging.py

A module for handling text i/o.

Includes logging configurations and welcome message.

"""
import logging
import sys
import textwrap
import craption.config as cfg


##########################################################################
# Settings, where appropriate import from config.py
# If printable_area is not at least around 100 characters, textwrap.wrap
# produces erratic line breaks, and looks a bit crap.
##########################################################################
total_columns = cfg.total_columns
caller_padding = cfg.caller_padding
time_padding = cfg.time_padding
total_padding = caller_padding + time_padding
printable_area = total_columns - total_padding

##########################################################################
# Logging implementations.
##########################################################################
divider_flag = 'divider_replace_me'

def divider():
    return divider_flag


class LogFormatter(logging.Formatter):
    """Nice-looking formatter for logging"""
    @staticmethod
    def set_caller_name(name):
        """
        A function to take the caller string (the bit in the log printout
        which tells you where the call was made) and format it, to make sure
        it fits the rest of the printout.
        """
        # First of all, if we are running a header, return an empty string.
        if 'decorator_divider' in name:
            return ''
        # If any unhelpful terms appear chuck them.
        terms_to_remove = ['.<module>', '.__init__']
        for term in terms_to_remove:
            if term in name:
                name = name.replace(term, '')
        # If there is enough room for the full caller designation, return it.
        if len(name) <= caller_padding:
            return name
        return '...' + name[-caller_padding + 2:]

    def format(self, record):
        s = super(LogFormatter, self).format(record)

        if divider_flag in s:
            s = total_columns * '-'
            return s

        split_list = s.split('|')
        # Find exceptions and remove them from the string, for separate handling.
        if '\nTraceback (most recent call last)' in split_list[-1]:
            s = split_list[-1]
            i = s.find('\nTraceback (most recent call last)')
            split_list[-1] = s[:i]
            exception_message = s[i:]
            print(data.RED+exception_message+'\n')

        # Get the timestamp.
        timestamp = split_list[0]
        caller_name = self.set_caller_name(split_list[1])
        print_statement = split_list[2]

        # Make a list of print_statements that need to be prepended
        # with the time and caller info.
        # Take off some characters because textwrap.wrap soft wraps.
        s_list = textwrap.wrap(print_statement, printable_area-3,
                               break_long_words=True)

        # Now we need to construct each individual printout string from the time,
        # caller, and print info.
        for index, printout in enumerate(s_list):
            # If it's the first line, add the timestamp and caller.
            if index == 0:
                # pad the caller name to be the required length
                caller_name = caller_name.rjust(caller_padding + 1)
                the_string = timestamp + '| ' + caller_name + '|' + printout
            else:
                the_string = (len(timestamp)*' ' + '| ' +
                              ''.rjust(caller_padding + 1) + '| ' + printout)
            s_list[index] = the_string

        s = '\n'.join(s_list).strip(" ")
        return s


def headed_log(func):
    """A decorator for header breaks in stdout."""
    def decorator_divider(*args, **kwargs):
        func(craption.tools.logging.divider())
        func(*args, **kwargs)
        func(craption.tools.logging.divider())
    return decorator_divider


class LoggableMixin:
    """
    A class to inherit from, such that the logger knows the
    name of the class making the call.
    """
    @property
    def log(self):
        """Property to return a mixin logger."""
        my_log = logging.getLogger('classlog')
        my_log.name = self.__class__.__name__
        return my_log


def func_log(func):
    """
    Decorator for functions which override the log in the module
    namespace to use the correct name.
    """
    def wrapper(*args, **kwargs):
        log = logging.getLogger(sys.modules[func.__module__].__name__)
        log.setLevel(cfg.verbosity)
        r = func(*args, **kwargs)
        return r
    return wrapper
