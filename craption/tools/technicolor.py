name = "technicolor"

import ctypes
import functools
import inspect
import logging
import os
import platform
import sys

def run_from_ipython():
    try:
        __IPYTHON__
        return True
    except NameError:
        return False

use_alt_colors = run_from_ipython()

class ColorisingStreamHandler(logging.StreamHandler):
    # color names to indices
    color_map = {
        "black": 0,
        "red": 1,
        "green": 2,
        "yellow": 3,
        "blue": 4,
        "magenta": 5,
        "cyan": 6,
        "white": 7 # if use_alt_colors else 0,
    }

    # level colour specifications
    # syntax: logging.level: (background color, foreground color, bold)
    level_map = {
        'local_filename': (None, "cyan", False),
        'remote_filename': (None, "red", False),
        logging.DEBUG: (None, "blue", False),
        logging.INFO: (None, "white", False),
        logging.WARNING: (None, "yellow", False),
        logging.ERROR: (None, "red", False),
        logging.CRITICAL: ("red", "white", True),
    }

    # control sequence introducer
    CSI = "\x1b["

    # normal colours
    reset = "\x1b[0m"

    def istty(self):
        isatty = getattr(self.stream, "isatty", None)
        return isatty and isatty()

    def emit(self, record):
        try:
            message = self.format(record)
            stream = self.stream
            if not self.istty:
                stream.write(message)
            else:
                self.output_colorized(message)
            stream.write(getattr(self, "terminator", "\n"))
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def output_colorized(self, message):
        self.stream.write(message)

    def colorize(self, message, record):
        levelno = None
        # print(record, isinstance(record, str))
        if isinstance(record, str):
            levelno = record
        else:
            levelno = record.levelno
        if levelno in self.level_map:
            background_color, \
            foreground_color, \
            bold = self.level_map[levelno]
            parameters = []
            if background_color in self.color_map:
                parameters.append(str(self.color_map[background_color] + 40))
            if foreground_color in self.color_map:
                parameters.append(str(self.color_map[foreground_color] + 30))
            if bold:
                parameters.append("1")
            if parameters:
                message = "".join((
                    self.CSI,
                    ";".join(parameters),
                    "m",
                    message,
                    self.reset
                ))
        return message

    # def formatException(self, ei):
    #     result = super().formatException(ei)
    #     return(result)

    def format(self, record):
        message = logging.StreamHandler.format(self, record)
        # print('tc:', record)
        if self.istty:
            # Colorise all multiline output.
            parts = message.split("\n", 1)
            for index, part in enumerate(parts):
                parts[index] = self.colorize(part, record)
            message = "\n".join(parts)
            # Now colorise all file names.
            parts = message.split(' ')
            # print(parts)
            for index, part in enumerate(parts):
                if not part: continue
                if part[0] == '/':
                    parts[index] = self.colorize(part, 'local_filename')
                elif part.startswith('https'):
                    parts[index] = self.colorize(part, 'remote_filename')
            message = " ".join(parts)
        return (message)


def log(function):
    @functools.wraps(function)
    def decoration(
            *args,
            **kwargs
    ):
        # Get the names of all of the function arguments.
        arguments = inspect.getcallargs(function, *args, **kwargs)
        logging.debug(
            "function '{function_name}' called by '{caller_name}' with arguments:"
            "\n{arguments}".format(
                function_name=function.__name__,
                caller_name=inspect.stack()[1][3],
                arguments=arguments
            ))
        result = function(*args, **kwargs)
        logging.debug("function '{function_name}' result: {result}\n".format(
            function_name=function.__name__,
            result=result
        ))

    return (decoration)
