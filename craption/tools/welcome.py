# -*- coding: utf-8 -*-
"""
welcome.py

Contains the design of the welcome message

"""
import random
import sys

import textwrap

import craption
import craption.config as cfg

from datetime import datetime

###################################################################
# Codes for text colours and style
###################################################################
DARK_RED = "\033[0;31m"
RED = "\033[1;31m"
YELLOW = "\033[1;33m"
WHITE = "\033[1;37m"
BLUE = "\033[1;34m"
CYAN = "\033[1;36m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"
BOLD = "\033[;1m"
REVERSE = "\033[;7m"

total_columns = cfg.total_columns

def ascii_logo():
    """
    Returns:
        string containing ascii art logo

    Generated with:
        http://patorjk.com/software/taag/#p=display&f=Crawford2&t=Craption
    """
    str = """
   __  ____    ____  ____  ______  ____  ___   ____
   /  ]|    \  /    ||    \|      ||    |/   \ |    \\
  /  / |  D  )|  o  ||  o  )      | |  ||     ||  _  |
 /  /  |    / |     ||   _/|_|  |_| |  ||  O  ||  |  |
/   \_ |    \ |  _  ||  |    |  |   |  ||     ||  |  |
\     ||  .  \|  |  ||  |    |  |   |  ||     ||  |  |
 \____||__|\_||__|__||__|    |__|  |____|\___/ |__|__|

    """
    return str


##########################################################################
# Welcome message colours.
##########################################################################
def welcome_message(on=True):
    """ Prints a welcome message for the app to stdout

        Args:
            on {bool} : whether to display by default
        Returns:
            None
    """
    if not cfg.has_displayed_welcome:
        if not on:
            return
        sys.stdout.write(GREEN)
        sys.stdout.write('$' * total_columns + '\n')
        sys.stdout.write('$' * total_columns + '\n')
        sys.stdout.write(BOLD + GREEN)
        welcome = 'Welcome to version {} of...'.format(craption.__version__)
        dt = datetime.utcnow().strftime('%D %T')
        logo_base = ascii_logo()

        sys.stdout.write('$$$' + ''.center(total_columns - 6, ' ') + '$$$\n')
        sys.stdout.write('$$$' + RED + welcome.center(total_columns - 6, ' ')
                         + GREEN + '$$$\n')

        for logo in logo_base.splitlines():
            sys.stdout.write('$$$' + RED + logo.center(total_columns - 6, ' ')
                             + GREEN + '$$$\n')
        sys.stdout.write(
            '$$$' + RED + '\x1B[3m' +
            '"...where your voice is always misheard"'.center(total_columns-6 , ' ')
            + '\x1B[23m' + GREEN + '$$$\n'
            )
        sys.stdout.write('$$$' + ''.center(total_columns - 6, ' ') + '$$$\n')
        sys.stdout.write('$$$' + RED + dt.center(total_columns - 6, ' ')
                         + GREEN + '$$$\n')
        sys.stdout.write('$$$' + ''.center(total_columns - 6, ' ') + '$$$\n')

        sys.stdout.write(GREEN)
        sys.stdout.write('$' * total_columns + '\n')
        sys.stdout.write('$' * total_columns + '\n')
        sys.stdout.write(RESET)
        cfg.has_displayed_welcome = True
        return
