""" Contains functions and classes for parsing grammar and looking up word
    definitions. uses the NLTK library.
"""
import nltk

from cached_property import cached_property
from nltk.corpus import treebank, wordnet
from nltk.tokenize import sent_tokenize, word_tokenize, PunktSentenceTokenizer
#from PyDictionary import PyDictionary

from craption.tools.logging import LoggableMixin

# offer help on meanings of parts-of-speech (POS) tags
# https://medium.com/@gianpaul.r/tokenization-and-parts-of-speech-pos-tagging-in-pythons-nltk-library-2d30f70af13b
pos_tag_meanings = {
    'CC' : 'coordinating conjunction',
    'CD' : 'cardinal digit',
    'DT' : 'determiner',
    'EX' : 'existential there (like: “there is” … think of it like “there exists”)',
    'FW' : 'foreign word',
    'IN' : 'preposition/subordinating conjunction',
    'JJ' : 'adjective ‘big’',
    'JJR' : 'adjective, comparative ‘bigger’',
    'JJS' : 'adjective, superlative ‘biggest’',
    'LS' : 'list marker 1)',
    'MD' : 'modal could, will',
    'NN' : 'noun, singular ‘desk’',
    'NNS' : 'noun plural ‘desks’',
    'NNP' : 'proper noun, singular ‘Harrison’',
    'NNPS' : 'proper noun, plural ‘Americans’',
    'PDT' : 'predeterminer ‘all the kids’',
    'POS' : 'possessive ending parent’s',
    'PRP' : 'personal pronoun I, he, she',
    'PRP$' : 'possessive pronoun my, his, hers',
    'RB' : 'adverb very, silently,',
    'RBR' : 'adverb, comparative better',
    'RBS' : 'adverb, superlative best',
    'RP' : 'particle give up',
    'TO' : 'to go ‘to’ the store.',
    'UH' : 'interjection, errrrrrrrm',
    'VB' : 'verb, base form take',
    'VBD' : 'verb, past tense took',
    'VBG' : 'verb, gerund/present participle taking',
    'VBN' : 'verb, past participle taken',
    'VBP' : 'verb, sing. present, non-3d take',
    'VBZ' : 'verb, 3rd person sing. present takes',
    'WDT' : 'wh-determiner which',
    'WP' : 'wh-pronoun who, what',
    'WP$' : 'possessive wh-pronoun whose',
    'WRB' : 'wh-abverb where, when'
}


def wordnet_pos_code(tag):
    """ map obscure POS tags ontp appropriate wordnet types """
    if tag.startswith('NN'):
        return wordnet.NOUN
    elif tag.startswith('VB'):
        return wordnet.VERB
    elif tag.startswith('JJ'):
        return wordnet.ADJ
    elif tag.startswith('RB'):
        return wordnet.ADV
    else:
        return ''


def wordnet_pos_label(tag):
    """ replace obscure POS tags with user-understandable grammatical terms """
    if tag.startswith('NN'):
        return "Noun"
    elif tag.startswith('VB'):
        return "Verb"
    elif tag.startswith('JJ'):
        return "Adjective"
    elif tag.startswith('RB'):
        return "Adverb"
    else:
        return tag


class ParsedText(LoggableMixin):

    def __init__(self, text):
        self.text = text

    @cached_property
    def tokenised_sentences(self) -> list:
        """ guess sentence boundaries in monolithic text block

            Returns:
                a list of sentences
        """
        return sent_tokenize(self.text)

    @cached_property
    def unique_words(self) -> list:
        """ list of the unique words in text (modulo punctuation)

            Returns:
                a list of unique words
        """
        return sorted(
            list(set(w for w in word_tokenize(self.text)
                     if len(w) > 1 or w.lower() in ['a','i']))
        )

    @cached_property
    def tagged_sentences(self) -> list:
        """ guess the grammatical structure of the sentences
            add parts-of-speech tags appropriately
            replace these where possible with 'noun', 'verb' etc.

            Returns:
                a list of sentences comprised of (word, grammar tag) pairs
        """
        sentences = [nltk.pos_tag(nltk.word_tokenize(sent))
                     for sent in self.tokenised_sentences]
        cleaned_sentences = [[] for _ in range(len(sentences))]
        for i in range(len(sentences)):
            s = sentences[i]
            for word, tag in s:
                if len(word) == 1 and word.lower() not in ['a','i']:
                    continue
                cleaned_sentences[i].append((word, wordnet_pos_label(tag)))
        return cleaned_sentences

# NYI:
"""
# Get a collection of synsets (synonym sets) for a word
synsets = wordnet.synsets( 'picture' )
print(synsets)
# Print the information
for synset in synsets:
    print("-" * 10)
    print("Name:", synset.name())
    print("Lexical Type:", synset.lexname())
    print("Synonyms:", synset.lemma_names())
    print("Definition:", synset.definition())
    print("Lemma names:", synset.lemma_names())
    print("Hyponyms:", [h.name().split('.')[0] for h in synset.hyponyms()])
    print("Hypernyms:", [h.name().split('.')[0] for h in synset.hypernyms()])
    print("Meronyms", [h.name().split('.')[0] for h in synset.part_meronyms()])
    print("Holonyms:", [h.name().split('.')[0] for h in synset.part_holonyms()])
    print("Entailments:", [h.name().split('.')[0] for h in synset.entailments()])
    for example in synset.examples():
        print("Example:", example)
"""
