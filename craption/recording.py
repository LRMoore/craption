# coding=utf-8
"""
Contains utilities and classes for recording audio samples,
managing their properties and metadata and storing this in an SQLite database.
"""
import os
import shutil
import re
import wave
import uuid
import datetime
import time
import textwrap

import pandas as pd
import numpy as np
import sqlalchemy as sa
import pyaudio

from ctypes import *
from contextlib import contextmanager
from time import perf_counter as pc

from tabulate import tabulate

from craption.tools.recorder import Recorder, noalsaerr
from craption.tools.logging import LoggableMixin
import craption.config as cfg


def indent_transcript(text):
    """ add newlines to nicely format transcript output """
    if isinstance(text, str):
        return textwrap.fill(text, cfg.total_columns - 33)
    return ''


def get_unique_path(fname_path):
    """
    Get path to a filename which does not exist by adding an incrementing suffix

    Examples
    --------
    >>> get_unique_path('/etc/issue')
    '/etc/issue_1'
    """
    if not os.path.exists(fname_path):
        return fname_path
    filename, file_extension = os.path.splitext(fname_path)
    i = 1
    new_fname = "{}_{}{}".format(filename, i, file_extension)
    while os.path.exists(new_fname):
        i += 1
        new_fname = "{}_{}{}".format(filename, i, file_extension)
    return new_fname


class Recording(LoggableMixin):
    """
    Class to record audio samples as .wav files, play them back and
    assign properties such as the speaker name and an accurate transcription
    """

    def __init__(self,
                 tag='untitled', # identifying string
                 channels=1, # mono
                 rate=16000, # 16 kHz sample rate
                 frames_per_buffer=1024,
                 max_rec_len=30, # seconds
                 media_path=cfg.recording_dir):
        self.channels = channels
        self.rate = rate
        self.frames_per_buffer = frames_per_buffer
        self.max_rec_len = 30
        self.recorded_at = None
        # toggle when record method called to prevent confusion with prev recs
        self.tag = tag
        self.media_path = media_path
        self.saved_path = None
        self.duration = 0.
        self.speaker = None
        self.user_transcript = None
        self.predicted_transcript = None
        self._unique_tag = None
        self._uuid4 = str(uuid.uuid4())

    def __repr__(self):
        headers = ['Recording info']
        table = [
            ['unique tag', self.unique_tag],
            ['tag', self.tag],
            ['path', self.path],
            ['recorded at', self.recorded_at],
            ['duration (s)', f'{self.duration:{10}.{3}}'],
            ['sample rate (Hz)', self.rate],
            ['speaker', self.speaker],
            ['transcript (user-provided)', indent_transcript(self.user_transcript)],
            ['transcript (predicted)', indent_transcript(self.predicted_transcript)]
        ]
        text = tabulate(table, headers, tablefmt="fancy_grid")
        output_lines = []
        for line in text.splitlines():
            output_lines.append(line.center(cfg.total_columns, ' '))
        return '\n' + '\n'.join(output_lines) + '\n'

    @property
    def recorded(self):
        return True if self.recorded_at is not None else False

    @property
    def uuid4(self):
        return self._uuid4

    @property
    def user_transcript(self):
        return self._user_transcript

    @user_transcript.setter
    def user_transcript(self, value):
        self.log.debug(f"update user transcript to: {value}")
        self._user_transcript = value

    @property
    def predicted_transcript(self):
        return self._predicted_transcript

    @predicted_transcript.setter
    def predicted_transcript(self, value):
        self.log.debug(f"update predicted transcript to: {value}")
        self._predicted_transcript = value

    @property
    def tag(self):
        return self._tag

    @property
    def unique_tag(self):
        return self._unique_tag

    @tag.setter
    def tag(self, value):
        """ Tag setter logic - append unique suffix
            Upon changing the tag of an existing recording, rename the file
        """
        value = value.replace(' ','_')
        self.log.debug(f"update tag to {value}")
        self._tag = value
        if self.recorded:
            curr_path = self.unique_path()
            if self.saved_path != curr_path:
                self.log.warning("tag updated after file saved.")
                self.log.warning(f"moving sample from {self.saved_path} "
                                 f"to {curr_path}...")
                self._path = curr_path
                shutil.move(self.saved_path, curr_path)
                self.saved_path = curr_path

    def unique_path(self) -> str:
        """ Create a unique filename for self.tag within self.media_path
            by appending an appropriate suffix
        """
        path = os.path.join(self.media_path, self.tag + '.wav')
        path = get_unique_path(path)
        self._unique_tag = os.path.split(path)[-1].rstrip('.wav')
        return path

    @property
    def path(self) -> str:
        """ Return a unique filepath if the recording has not yet been saved
        """
        if not self.recorded:
            self._path = self.unique_path()
        return self._path

    def record(self, overwrite=False) -> None:
        """ Record an audio sample to media_path/tag(_N).wav

            press ctrl+c (KeyboardInterrupt) to stop
        """
        if self.recorded and not overwrite:
            self.log.info("Attempted record when audio file already exists and "
                            "overwrite=False. Ignoring...")
            return
        rec = Recorder(channels=self.channels,
                       rate=self.rate,
                       frames_per_buffer=self.frames_per_buffer)
        with rec.open(self.path, 'wb') as recfile2:
            try:
                self.log.debug("start recording audio...")
                t0 = pc()
                recfile2.start_recording()
                time.sleep(self.max_rec_len)
            except KeyboardInterrupt:
                pass
            finally:
                self.log.debug("stop recording audio...")
                recfile2.stop_recording()
                self.duration = pc() - t0
                self.recorded_at = str(datetime.datetime.now())
                self.saved_path = self.path

    def play(self) -> None:
        """ Play the recorded audio sample via PyAudio w/ default output device
        """
        if not self.recorded:
            self.log.info("Unable to play sample as nothing has been recorded yet!")
            return
        # Open the sound file
        try:
            wf = wave.open(self.path, 'rb')
        except IOError:
            self.log.error(f"file {self.path} not found! has it been deleted?")
            return
        # Create an interface to PortAudio
        with noalsaerr():
            p = pyaudio.PyAudio()
        # Open a .Stream object to write the WAV file to
        # 'output = True' indicates that the sound will be played rather than recorded
        stream = p.open(format = p.get_format_from_width(wf.getsampwidth()),
                        channels = wf.getnchannels(),
                        rate = wf.getframerate(),
                        output = True)
        # Read data in chunks
        data = wf.readframes(self.frames_per_buffer)
        # Play the sound by writing the audio data to the stream
        while len(data) > 0:
            stream.write(data)
            data = wf.readframes(self.frames_per_buffer)
        # Close and terminate the stream
        stream.close()
        p.terminate()


class RecordingDatabase(LoggableMixin):
    """
    Database class which uses SQLAlchemy + pandas to store and organise
    the metadata and file path information associated with each audio recording

    Audio samples' metadata is stored in a pandas dataframe which is persisted
    as an SQLite table via the save/load methods
    """
    # table columns
    metadata_columns = ['channels', 'rate', 'path', 'recorded_at', 'duration',
                        'tag', 'unique_tag', 'user_transcript', 'predicted_transcript',
                        'speaker', 'uuid4']

    def __init__(self,
                 db:'string path or sqlalchemy connection object',
                 name:'sql table name prefix'='samples',
                 index_col:'column to set as key/index'='unique_tag',
                 verbose=False):
        self.name = name
        self._ensure_db_connection(db)
        self.verbose = verbose
        self.index_col = index_col
        try:
            self.load()
        except sa.exc.OperationalError as oe:
            self.log.warning(f"WARNING: no existing table {self.name} found. "
                             "Creating empty table...")
            self._df = pd.DataFrame(
                columns=self.metadata_columns).set_index(self.index_col)
            self._df.name = self.name

    #def __getattr__(self, attr):
    #    if attr in self._df.index:
    #        return self._df.loc[attr]
    #    else:
    #        return super().__getattribute__(attr)

    def _ensure_db_connection(self, db):
        if isinstance(db, sa.engine.base.Connection):
            self.db_connection = db
        elif isinstance(db, str):
            self.log.debug(f"creating SQLAlchemy connection engine for {db}")
            self._ensure_directory(db)
            engine = sa.create_engine(f'sqlite:///{db}',  echo=verbose)
            self.db_connection = engine.connect()
        else:
            raise TypeError("Invalid arg: db should be a string pointing to an SQLite "
                            "database or a sqlalchemy.engine.base.Connection object.")

    def _ensure_directory(self, path):
        directory = os.path.dirname(path)
        if not os.path.exists(directory):
            self.log.debug(f"creating directory {path}")
            os.makedirs(directory)

    def add_recording(self, rec):
        tag = rec.unique_tag
        self.log.debug(f"adding recording with unique tag {tag} to database")
        self.log.debug(f"overwrite duplicate with same uuid4...")
        self._df = self._df[self._df.uuid4 != rec.uuid4]
        for col in self.metadata_columns:
            if col != self.index_col:
                self._df.loc[tag, col] = getattr(rec, col)

    def save(self) -> None:
        self.log.debug("saving metadata to SQLite database...")
        self._df.to_sql(name=self.name, con=self.db_connection, if_exists='replace')

    def load(self) -> None:
        self.log.debug("loading metadata from SQLite database...")
        self._df = pd.read_sql(self.name, con=self.db_connection)
        self._df.name = self.name
        self._df.set_index(self.index_col, inplace=True)

    def __repr__(self) -> str:
        """ print (an abridged version of) a representation of the database
            with the unique tags, durations and speakers of all recs present
        """
        table = self._df[['speaker', 'recorded_at', 'duration']]
        text = tabulate(table, headers="keys", tablefmt="fancy_grid")
        output_lines = []
        for line in text.splitlines():
            output_lines.append(line.center(cfg.total_columns, ' '))
        return '\n' + '\n'.join(output_lines) + '\n'

    def get_recording(self, unique_tag) -> Recording:
        """ construct a Recording object given a unique_tag and return it
        """
        try:
            row = self._df.loc[unique_tag]
        except KeyError:
            self.log.debug(f"failed to find entry with unique tag {unique_tag}")
            raise
        self.log.debug(f"reconstructing recording {unique_tag} from database")
        rec = Recording.__new__(Recording)
        # reconstruct public attributes directly
        for pa in ['channels', 'rate', 'recorded_at', 'duration', 'speaker']:
            setattr(rec, pa, getattr(row, pa))
        rec.frames_per_buffer = 1024
        rec.max_rec_len = 30
        # and private attributes
        rec._user_transcript = row.user_transcript
        rec._predicted_transcript = row.predicted_transcript
        rec._path = row.path
        rec._tag = row.tag
        rec._unique_tag = row.name
        rec._uuid4 = row.uuid4
        return rec
